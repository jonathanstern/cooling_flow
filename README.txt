Package for deriving steady-state solutions of radiatively cooling gas in a constant gravitational potential
Methodology and physics described in
http://ui.adsabs.harvard.edu/abs/2019MNRAS.488.2549S
and
https://arxiv.org/abs/1909.07402

Files:
example.ipynb		Jupyter Notebook with explanations and a usage example
cooling_flow.py		Main module for integration of flow equations. 
HaloPotential.py 	Several examples of gravitational potentials
WiersmaCooling.py	Wraps the Wiersma et al. (2009) cooling functions
cooling/		Hdf5 files for the Wiersma+09 cooling tables


